#!/bin/bash

set -e

cp $SITE_FOLDER/_ss_environment.php /var/www/_ss_environment.php
chmod 644 /var/www/_ss_environment.php
ln -s $SITE_FOLDER/assets /var/www/assets
